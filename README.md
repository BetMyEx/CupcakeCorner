# CupcakeCorner
CupcakeCorner is a multi-screen app for ordering cupcakes.
## Used features
- make classes conform to Codable when they have @Published properties
- send and receive the order data from the internet 
-validate forms
## App's overview
![screen-gif](./CupcakeCorner/Preview/CupcakeCorner.gif)
