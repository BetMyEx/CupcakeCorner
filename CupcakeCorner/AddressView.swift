//
//  AddressView.swift
//  CupcakeCorner
//
//  Created by Admin on 04.03.2022.
//

import SwiftUI

struct AddressView: View {
    @ObservedObject var order: Order
    
    var body: some View {
        Form {
            Section {
                TextField("Name", text: $order.newOrderSctruct.name)
                TextField("Street address", text: $order.newOrderSctruct.streetAddress)
                TextField("City", text: $order.newOrderSctruct.city)
                TextField("Zip", text: $order.newOrderSctruct.zip)
            }
            
            Section {
                NavigationLink {
                    CheckoutView(order: order)
                } label: {
                    Text("Check out")
                }
            }
            .disabled(order.newOrderSctruct.hasValidAddress == false)
        }
        .navigationTitle("Delivery details")
        .navigationBarTitleDisplayMode(.inline)
    }
}

struct AddressView_Previews: PreviewProvider {
    static var previews: some View {
        NavigationView {
        AddressView(order: Order())
        }
    }
}
