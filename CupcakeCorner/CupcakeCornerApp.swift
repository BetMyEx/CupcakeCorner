//
//  CupcakeCornerApp.swift
//  CupcakeCorner
//
//  Created by Admin on 04.03.2022.
//

import SwiftUI

@main
struct CupcakeCornerApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
