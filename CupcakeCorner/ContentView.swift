//
//  ContentView.swift
//  CupcakeCorner
//
//  Created by Admin on 04.03.2022.
//

import SwiftUI

struct ContentView: View {
    @StateObject var order = Order()
    
    
    var body: some View {
        NavigationView {
            Form {
                Section {
                    Picker("Select your cake type", selection: $order.newOrderSctruct.type){
                        ForEach(NewOrderSctruct.types.indices) {
                            Text(NewOrderSctruct.types[$0])
                        }
                    }
                    Stepper("Number of cakes: \(order.newOrderSctruct.quantity)", value: $order.newOrderSctruct.quantity, in: 3...20)
                }
                Section {
                    Toggle("Any special requests?", isOn: $order.newOrderSctruct.specialRequestEnabled.animation())
                    
                    if order.newOrderSctruct.specialRequestEnabled {
                        Toggle("Add extra frosting", isOn: $order.newOrderSctruct.extraFrosting)
                        Toggle("Add extra sprinkles", isOn: $order.newOrderSctruct.addSprinkles)
                    }
                }
                Section {
                    NavigationLink {
                        AddressView(order: order)
                    } label: {
                        Text("Delivery details")
                    }
                    }
                }
                
            }
            .navigationTitle("Cupcake Corner")
        }
    }


struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

